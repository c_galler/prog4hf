import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ReservationCreateComponent } from './reservations/reservation-create/reservation-create.component';
import { TableListComponent } from './tables/table-list/table-list.component';
import { AdminGuard } from './guards/admin.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoyaltyCardListComponent } from './loyalty-cards/loyalty-card-list/loyalty-card-list.component';
import { ManagementUserTableComponent } from './admin/userManagement/management-user-table/management-user-table.component';
import { AdminReservationListComponent } from './admin/reservationManagement/admin-reservation-list/admin-reservation-list.component';
import { AdminReservationDetailComponent } from './admin/reservationManagement/admin-reservation-detail/admin-reservation-detail.component';
import { AdminReservationEditComponent } from './admin/reservationManagement/admin-reservation-edit/admin-reservation-edit.component';

const routes: Routes = [
  { path: 'api/admin/userManagement/users',  component: ManagementUserTableComponent, canActivate: [AdminGuard]},
  { path: 'api/admin/reservationManagement/reservations/:id/edit',  component: AdminReservationEditComponent, canActivate: [AdminGuard]},
  { path: 'api/admin/reservationManagement/reservations/:id',  component: AdminReservationDetailComponent, canActivate: [AdminGuard]},
  { path: 'api/admin/reservationManagement/reservations',  component: AdminReservationListComponent, canActivate: [AdminGuard]},
  { path: 'api/admin/tableManagement/tables',  component: TableListComponent, canActivate: [AdminGuard]},
  { path: 'api/admin/loyaltyCardManagement/loyaltyCards',  component: LoyaltyCardListComponent, canActivate: [AdminGuard]},
  { path: 'home',  component: HomeComponent},
  { path: 'login',  component: LoginComponent},
  { path: 'tableReservation',  component: ReservationCreateComponent},
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
