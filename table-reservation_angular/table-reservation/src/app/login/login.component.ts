import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  username: string = '';
  password: string = '';
  error: boolean = false;
  logout: boolean = false;

  constructor(private authService: AuthService, private router: Router, private location: Location) {}

  login() {
    this.authService.login(this.username, this.password).subscribe({
        next: (v) => this.router.navigate(['/']),
        error: (e) => console.error('Login failed', e),
        complete: () => this.refreshPage()
    });
  }

  refreshPage() {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([decodeURI(this.location.path())]);
    });
  }
}
