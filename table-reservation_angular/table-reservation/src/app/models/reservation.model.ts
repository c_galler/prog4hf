import { RestaurantTable } from "./restaurantTable.model";
import { AppUser } from "./appUser.model";

export interface Reservation {
    id?: number;
    appUser: AppUser;
    date: Date;
    startTime: string;
    endTime: string;
    numberOfGuests: number;
    timeOfBooking: Date;
    comment: string;
    restaurantTables: RestaurantTable[];
  }