export interface RestaurantTable {
    id: number;
    tableNumber: number;
    maxNumberOfSeats: number;
}