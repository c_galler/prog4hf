import { AppUser } from "./appUser.model";
import { LoyaltyCard } from "./loyaltyCard.model";
import { Role } from "./role.model";

export interface RegisteredAppUser extends AppUser {
    userName: string;
    password: string;
    address?: string;
    city?: string;
    postCode?: string;
    dateOfBirth?: Date;
    loyaltyCard?: LoyaltyCard; 
    enabled?: boolean;
    roles?: Role[]; 
  }