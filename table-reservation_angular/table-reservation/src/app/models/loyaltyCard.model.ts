export interface LoyaltyCard {
    cardNumber: number;
    points: number;
    expirationDate: string;
}