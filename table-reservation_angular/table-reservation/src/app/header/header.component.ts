import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent  implements OnDestroy {
  isAdminUser: boolean = false;
  isLoggedInUser: boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(public authService: AuthService, private router: Router) {
    this.checkAdminStatus();
    this.checkLoginStatus();
  }

  checkAdminStatus() {
    this.authService.isAdmin()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(isAdmin => {
        this.isAdminUser = isAdmin;
      });
  }

  checkLoginStatus() {
    this.authService.isLoggedIn()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(isLoggedIn => {
        this.isLoggedInUser = isLoggedIn;
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  
  logout() {
    this.authService.logout()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: () => {
          console.log('Logged out successfully');
          this.router.navigate(['/login']);
        },
        error: (error) => console.error('Logout failed', error)
      });
  }


}

