import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ReservationCreateComponent } from './reservations/reservation-create/reservation-create.component';
import { ReservationListComponent } from './reservations/reservation-list/reservation-list.component';
import { ReservationEditComponent } from './reservations/reservation-edit/reservation-edit.component';
import { ReservationDetailComponent } from './reservations/reservation-detail/reservation-detail.component';
import { LoyaltyCardEditComponent } from './loyalty-cards/loyalty-card-edit/loyalty-card-edit.component';
import { LoyaltyCardListComponent } from './loyalty-cards/loyalty-card-list/loyalty-card-list.component';
import { LoyaltyCardDetailComponent } from './loyalty-cards/loyalty-card-detail/loyalty-card-detail.component';
import { FooterComponent } from './footer/footer.component';
import { ProfileComponent } from './profile/profile.component';
import { RoleDetailComponent } from './roles/role-detail/role-detail.component';
import { RoleListComponent } from './roles/role-list/role-list.component';
import { RoleEditComponent } from './roles/role-edit/role-edit.component';
import { TableDetailComponent } from './tables/table-detail/table-detail.component';
import { TableEditComponent } from './tables/table-edit/table-edit.component';
import { TableListComponent } from './tables/table-list/table-list.component';
import { TableCreateComponent } from './tables/table-create/table-create.component';
import { AppUserDetailComponent } from './users/app-user-detail/app-user-detail.component';
import { AppUserEditComponent } from './users/app-user-edit/app-user-edit.component';
import { AppUserListComponent } from './users/app-user-list/app-user-list.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { provideHttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManagementUserDetailComponent } from './admin/userManagement/management-user-detail/management-user-detail.component';
import { ManagementUserTableComponent } from './admin/userManagement/management-user-table/management-user-table.component';
import { AdminReservationListComponent } from './admin/reservationManagement/admin-reservation-list/admin-reservation-list.component';
import { AdminReservationDetailComponent } from './admin/reservationManagement/admin-reservation-detail/admin-reservation-detail.component';
import { AdminReservationEditComponent } from './admin/reservationManagement/admin-reservation-edit/admin-reservation-edit.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { ConfirmDialogComponent } from './admin/reservationManagement/confirm-dialog/confirm-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule} from '@angular/material/card';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    ReservationCreateComponent,
    ReservationListComponent,
    ReservationEditComponent,
    ReservationDetailComponent,
    LoyaltyCardEditComponent,
    LoyaltyCardListComponent,
    LoyaltyCardDetailComponent,
    FooterComponent,
    ProfileComponent,
    RoleDetailComponent,
    RoleListComponent,
    RoleEditComponent,
    TableDetailComponent,
    TableEditComponent,
    TableListComponent,
    TableCreateComponent,
    AppUserDetailComponent,
    AppUserEditComponent,
    AppUserListComponent,
    RegistrationFormComponent,
    PageNotFoundComponent,
    ManagementUserDetailComponent,
    ManagementUserTableComponent,
    AdminReservationListComponent,
    AdminReservationDetailComponent,
    AdminReservationEditComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    MatListModule
  ],
  providers: [provideHttpClient(), provideAnimationsAsync()],
  bootstrap: [AppComponent]
})
export class AppModule { }
