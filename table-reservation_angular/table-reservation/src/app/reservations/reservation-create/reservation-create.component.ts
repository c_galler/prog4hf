import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ReservationService } from '../../services/reservation.service';
import { Reservation } from '../../models/reservation.model';
import { AppUser } from '../../models/appUser.model';

@Component({
  selector: 'app-reservation-create',
  templateUrl: './reservation-create.component.html',
  styleUrls: ['./reservation-create.component.scss']
})
export class ReservationCreateComponent {
  newReservation?: Reservation;
  newBookingOwner?: AppUser;
  timeOptions: string[];
  successMessage: string = "";

  reservationForm = this.fb.group({
    date: ['', Validators.required],
    startTime: ['', Validators.required],
    numberOfGuests: ['', [Validators.required, Validators.min(1), Validators.max(12)]],
    comment: [''],
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    phoneNumber: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]]
  });

  constructor(private fb: FormBuilder, private reservationService: ReservationService) {
    this.timeOptions = this.generateTimeOptions('12:00', '21:00', 30);
  }

  ngOnInit(): void { }

  generateTimeOptions(start: string, end: string, stepMinutes: number): string[] {
    const times = [];
    let [startHours, startMinutes] = start.split(':').map(Number);
    const [endHours, endMinutes] = end.split(':').map(Number);

    while (startHours < endHours || (startHours === endHours && startMinutes <= endMinutes)) {
      const formattedTime = `${this.pad(startHours)}:${this.pad(startMinutes)}`;
      times.push(formattedTime);
      startMinutes += stepMinutes;
      if (startMinutes >= 60) {
        startMinutes -= 60;
        startHours += 1;
      }
    }
    return times;
  }

  pad(num: number): string {
    return num.toString().padStart(2, '0');
  }

  makeReservation(): void {
    if (this.reservationForm.valid) {
      const formValues = this.reservationForm.value;

      const parsedDate = formValues.date ? new Date(formValues.date) : new Date();
      const parsedStartTime = formValues.startTime ?? '';
      const parsedNumberOfGuests = formValues.numberOfGuests ? Number(formValues.numberOfGuests) : 0;
      const parsedComment = formValues.comment ?? '';
      const parsedFirstName = formValues.firstName ?? '';
      const parsedLastName = formValues.lastName ?? '';
      const parsedEmail = formValues.email ?? '';
      const parsedPhoneNumber = formValues.phoneNumber ?? '';
      const endTime = this.calculateEndTime(parsedStartTime, parsedNumberOfGuests);

      this.newBookingOwner = {
        firstName: parsedFirstName,
        lastName: parsedLastName,
        email: parsedEmail,
        phoneNumber: parsedPhoneNumber
      }

      this.newReservation = {
        date: parsedDate,
        startTime: parsedStartTime,
        endTime: endTime,
        timeOfBooking: new Date(),
        numberOfGuests: parsedNumberOfGuests,
        appUser: this.newBookingOwner,
        restaurantTables: [],
        comment: parsedComment
      };

      this.reservationService.createReservation(this.newReservation, this.newBookingOwner).subscribe({
        complete: () => {
          console.log('Reservation Created!', this.newReservation);
          this.reservationForm.reset();
          this.successMessage = 'Reservation created successfully!';
        }
      });
    } else {
      this.reservationForm.markAllAsTouched();
    }
  }

  calculateEndTime(startTime: string, numberOfGuests: number): string {
    const [startHours, startMinutes] = startTime.split(':').map(Number);
    let duration = 90 + Math.floor((numberOfGuests - 2) / 2) * 30;

    let endHours = startHours + Math.floor(duration / 60);
    let endMinutes = startMinutes + (duration % 60);
    if (endMinutes >= 60) {
      endMinutes -= 60;
      endHours += 1;
    }

    return `${this.pad(endHours)}:${this.pad(endMinutes)}`;
  }
}
