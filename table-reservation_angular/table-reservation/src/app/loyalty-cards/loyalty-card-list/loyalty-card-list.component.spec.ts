import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoyaltyCardListComponent } from './loyalty-card-list.component';

describe('LoyaltyCardListComponent', () => {
  let component: LoyaltyCardListComponent;
  let fixture: ComponentFixture<LoyaltyCardListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoyaltyCardListComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoyaltyCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
