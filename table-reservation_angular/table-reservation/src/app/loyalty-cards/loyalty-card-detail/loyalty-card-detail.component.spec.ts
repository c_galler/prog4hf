import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoyaltyCardDetailComponent } from './loyalty-card-detail.component';

describe('LoyaltyCardDetailComponent', () => {
  let component: LoyaltyCardDetailComponent;
  let fixture: ComponentFixture<LoyaltyCardDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoyaltyCardDetailComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoyaltyCardDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
