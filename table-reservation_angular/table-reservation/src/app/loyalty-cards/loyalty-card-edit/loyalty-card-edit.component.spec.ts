import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoyaltyCardEditComponent } from './loyalty-card-edit.component';

describe('LoyaltyCardEditComponent', () => {
  let component: LoyaltyCardEditComponent;
  let fixture: ComponentFixture<LoyaltyCardEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoyaltyCardEditComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoyaltyCardEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
