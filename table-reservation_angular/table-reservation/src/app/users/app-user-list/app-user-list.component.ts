import { Component } from '@angular/core';
import { UserManagementService } from '../../services/userManagement.service';
import { AppUser } from '../../models/appUser.model';

@Component({
  selector: 'app-app-user-list',
  templateUrl: './app-user-list.component.html',
  styleUrl: './app-user-list.component.scss'
})
export class AppUserListComponent {
  users: AppUser[] = [];
  
  constructor(private userManagementService: UserManagementService) {
    this.userManagementService.getAll().subscribe((data: AppUser[]) => {
      this.users = data;
    })
  }
}
