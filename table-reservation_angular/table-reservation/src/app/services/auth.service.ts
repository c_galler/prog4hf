import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loginUrl = 'http://localhost:8080/login';
  private logoutUrl = 'http://localhost:8080/logout';
  private userUrl = 'http://localhost:8080/user';

  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<any> {
    const body = new URLSearchParams();
    body.set('username', username);
    body.set('password', password);

    return this.http.post(this.loginUrl, body.toString(), {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      withCredentials: true
    });
  }

  logout(): Observable<any> {
    return this.http.post(this.logoutUrl, {}, { withCredentials: true });
  }

  getUser(): Observable<any> {
    return this.http.get<any>(this.userUrl, { withCredentials: true });
  }

  isLoggedIn(): Observable<boolean> {
    return this.getUser().pipe(
      map(user => !!user.user)
    );
  }
  
  isAdmin(): Observable<boolean> {
    return this.getUser().pipe(
      map(user => user && user.authorities ? user.authorities.includes('ROLE_ADMIN') : false)
    );
  }
}

