import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Reservation } from '../models/reservation.model';
import { AppUser } from '../models/appUser.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  private reservationsUrl = `${environment.apiUrl}/reservations`;

  constructor(private http: HttpClient) {}

  getReservations(): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(this.reservationsUrl);
  }

  getReservationById(id: number): Observable<Reservation> {
    return this.http.get<Reservation>(`${this.reservationsUrl}/${id}`);
  }

  createReservation(reservation: Reservation, appUser: AppUser): Observable<Reservation> {
    const reservationData = {
        reservation: reservation,
        newUser: appUser
    };
    return this.http.post<Reservation>(this.reservationsUrl, reservationData);
  }

  updateReservation(id: number, reservation: Reservation): Observable<Reservation> {
    return this.http.put<Reservation>(`${this.reservationsUrl}/${id}`, reservation);
  }

  deleteReservation(id: number): Observable<void> {
    return this.http.delete<void>(`${this.reservationsUrl}/${id}`);
  }
}
