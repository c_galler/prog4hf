import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { AppUser } from '../models/appUser.model';

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {
  url: String = environment.apiUrl + '/admin/userManagement'

  constructor(private http: HttpClient) { }

  getAll(): Observable<AppUser[]> {
    return this.http.get<AppUser[]>(`${this.url}/users`);
  }

  getById(id: number): Observable<AppUser> { 
    return this.http.get<AppUser>(`${this.url}/users/${id}`);
  }

  create(user: AppUser): Observable<AppUser> {
    return this.http.post<AppUser>(`${this.url}/users`, user);
  }

  update(id: number, user: AppUser): Observable<AppUser> {
    return this.http.put<AppUser>(`${this.url}/users/${id}`, user);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.url}/users/${id}`);
  }
}
