import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementUserDetailComponent } from './management-user-detail.component';

describe('ManagementUserDetailComponent', () => {
  let component: ManagementUserDetailComponent;
  let fixture: ComponentFixture<ManagementUserDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ManagementUserDetailComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ManagementUserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
