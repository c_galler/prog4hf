import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementUserTableComponent } from './management-user-table.component';

describe('ManagementUserTableComponent', () => {
  let component: ManagementUserTableComponent;
  let fixture: ComponentFixture<ManagementUserTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ManagementUserTableComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ManagementUserTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
