import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminReservationDetailComponent } from './admin-reservation-detail.component';

describe('AdminReservationDetailComponent', () => {
  let component: AdminReservationDetailComponent;
  let fixture: ComponentFixture<AdminReservationDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminReservationDetailComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminReservationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
