import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReservationManagementService } from '../../../services/reservation-management.service';
import { Reservation } from '../../../models/reservation.model';
import { MatList, MatListItem } from '@angular/material/list';
import { MatCard, MatCardContent, MatCardTitle, MatCardHeader } from '@angular/material/card';

@Component({
  selector: 'app-admin-reservation-detail',
  templateUrl: './admin-reservation-detail.component.html',
  styleUrls: ['./admin-reservation-detail.component.scss']
})
export class AdminReservationDetailComponent implements OnInit {
  reservation?: Reservation;

  constructor(private route: ActivatedRoute, private reservationManagementService: ReservationManagementService, private router: Router) { }

  ngOnInit(): void {
    this.setClickedReservation();
  }

  goBack() {
    this.router.navigateByUrl('/api/admin/reservationManagement/reservations');;
  }

  setClickedReservation(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if(id != null) {
      this.reservationManagementService.getReservationById(+id)?.subscribe(reservation => {
        this.reservation = reservation;
      }, error => {
        console.error('Error fetching reservation: ', error);
      });
    }
  }
}