import { Component, Input, SimpleChanges } from '@angular/core';
import { Reservation } from '../../../models/reservation.model';
import { FormBuilder, Validators } from '@angular/forms';
import { ReservationManagementService } from '../../../services/reservation-management.service';
import { AppUser } from '../../../models/appUser.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';


@Component({
  selector: 'app-admin-reservation-edit',
  templateUrl: './admin-reservation-edit.component.html',
  styleUrl: './admin-reservation-edit.component.scss'
})
export class AdminReservationEditComponent {
  @Input() reservation?: Observable<Reservation>;
  updatedReservation?: Reservation;
  owner?: AppUser;
  timeOptions: string[];
  reservationId!: number;
  successMessage: string = "";

  reservationForm = this.fb.group({
    date: ['', Validators.required],
    startTime: ['', Validators.required],
    numberOfGuests: ['', [Validators.required, Validators.min(1), Validators.max(12)]],
    comment: [''],
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    phoneNumber: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]]
  });

  constructor(private fb: FormBuilder, private reservationManagementService: ReservationManagementService, private route: ActivatedRoute, private router: Router) {
    this.timeOptions = this.generateTimeOptions('12:00', '21:00', 30);
  }

  
  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if(id) {
      this.reservationId = +id;
      this.reservationManagementService.getReservationById(this.reservationId).subscribe(reservation => {
        this.populateForm(reservation);
      });
    }
  }

  goBack(): void {
    this.router.navigateByUrl('/api/admin/reservationManagement/reservations')
  }

  updateReservation(): void {
    if (this.reservationForm.valid) {
      const formValues = this.reservationForm.value;
  
      const parsedDate = formValues.date ? new Date(formValues.date) : new Date();
      const parsedStartTime = formValues.startTime ?? '';
      const parsedNumberOfGuests = formValues.numberOfGuests ? Number(formValues.numberOfGuests) : 1;
      const parsedComment = formValues.comment ?? '';
      const parsedFirstName = formValues.firstName ?? '';
      const parsedLastName = formValues.lastName ?? '';
      const parsedEmail = formValues.email ?? '';
      const parsedPhoneNumber = formValues.phoneNumber ?? '';
      const endTime = this.calculateEndTime(parsedStartTime, parsedNumberOfGuests);
  
      this.owner = {
        firstName: parsedFirstName,
        lastName: parsedLastName,
        email: parsedEmail,
        phoneNumber: parsedPhoneNumber
      }
  
      this.updatedReservation = {
        date: parsedDate,
        startTime: parsedStartTime,
        endTime: endTime,
        timeOfBooking: new Date(),
        numberOfGuests: parsedNumberOfGuests,
        appUser: this.owner,
        restaurantTables: [],
        comment: parsedComment
      };

      console.log(this.owner, this.updatedReservation);
  
      this.reservationManagementService.updateReservation(this.reservationId, this.updatedReservation).subscribe({
        next: () => {
          console.log('Reservation Modified!', this.updatedReservation);
        },
        error: err => {
          console.error('Error updating reservation: ', err);
        },
        complete: () => {
          this.successMessage = "Reservation modified successfully!"
        }
      });
    }
  }

  populateForm(reservation: Reservation): void {
    console.log(reservation)

    this.reservationForm.patchValue({
      date: reservation.date?.toString(),
      startTime: reservation.startTime.slice(0,5),
      numberOfGuests: reservation.numberOfGuests?.toString(),
      comment: reservation.comment,
      firstName: reservation.appUser.firstName,
      lastName: reservation.appUser.lastName,
      phoneNumber: reservation.appUser.phoneNumber,
      email: reservation.appUser.email
    });
  }

  generateTimeOptions(start: string, end: string, stepMinutes: number): string[] {
    const times = [];
    let [startHours, startMinutes] = start.split(':').map(Number);
    const [endHours, endMinutes] = end.split(':').map(Number);

    while (startHours < endHours || (startHours === endHours && startMinutes <= endMinutes)) {
      const formattedTime = `${this.pad(startHours)}:${this.pad(startMinutes)}`;
      times.push(formattedTime);
      startMinutes += stepMinutes;
      if (startMinutes >= 60) {
        startMinutes -= 60;
        startHours += 1;
      }
    }
    return times;
  }

  pad(num: number): string {
    return num.toString().padStart(2, '0');
  }

  calculateEndTime(startTime: string, numberOfGuests: number): string {
    const [startHours, startMinutes] = startTime.split(':').map(Number);
    let duration = 90 + Math.floor((numberOfGuests - 2) / 2) * 30;

    let endHours = startHours + Math.floor(duration / 60);
    let endMinutes = startMinutes + (duration % 60);
    if (endMinutes >= 60) {
      endMinutes -= 60;
      endHours += 1;
    }

    return `${this.pad(endHours)}:${this.pad(endMinutes)}`;
  }

}
