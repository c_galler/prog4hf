import { Component } from '@angular/core';
import { Reservation } from '../../../models/reservation.model';
import { ReservationManagementService } from '../../../services/reservation-management.service';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-admin-reservation-list',
  templateUrl: './admin-reservation-list.component.html',
  styleUrl: './admin-reservation-list.component.scss'
})
export class AdminReservationListComponent {
  reservations: Reservation[] = [];
  displayedColumns: string[] = ['date', 'startTime', 'numberOfGuests', 'userName', 'userEmail', 'userPhone', 'tableNumber', 'comment', 'actions'];

  constructor(private reservationManagementService: ReservationManagementService, private router: Router, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loadReservations();
  }

  loadReservations(): void {
    this.reservationManagementService.getReservations().subscribe({
      next: (data: Reservation[]) => {
        this.reservations = data;
        console.log(this.reservations);
      },
      error: (e) => console.error('Error fetching reservations:', e)
    });
  }

  deleteReservation(id: number): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
    data: {
      title: 'Confirm Deletion',
      message: 'Are you sure you want to delete this reservation?'
    }
    });

    dialogRef.afterClosed().subscribe(result => {
    if (result === true) {
      this.reservationManagementService.deleteReservation(id).subscribe({
      next: () => {this.reservations = this.reservations.filter(reservation => reservation.id !== id);},
      error: (e) => console.error('Error deleting reservation:', e)
    });
    }
    });
  }


  editReservation(id: number): void {
    this.router.navigate(['api/admin/reservationManagement/reservations/', id, 'edit']).then(() => {
    }).catch((e) => {
      console.error('Error navigating to edit reservation:', e);
    });
  }
  
  viewDetails(id: number): void {
    this.router.navigate(['api/admin/reservationManagement/reservations/', id]).then(() => {
      // Handle successful navigation
    }).catch((e) => {
      console.error('Error navigating to reservation details:', e);
    });
  }

}
