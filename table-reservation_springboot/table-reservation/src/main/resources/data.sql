INSERT INTO LOYALTY_CARD(card_number, points, expiration_date, version, created, updated) VALUES
          (1001, 100, '2025-01-01', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
          (1002, 200, '2025-02-01', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO ROLES(id, name, version, created, updated) VALUES
           (1, 'ROLE_USER', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
           (2, 'ROLE_ADMIN', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO APP_USERS(id, first_name, last_name, email, phone_number, version, created, updated) VALUES
         (1001, 'John', 'Doe', 'john.doe@gmail.com', '06302182222', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
         (1002, 'Jane', 'Doe', 'jane.doe@gmail.com', '06302182223', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
         (1003, 'John', 'Smith', 'john.smith@gmail.com', '06302182224', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
         (1004, 'Jane', 'Smith', 'jane.smith@gmail.com', '06302182225', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO REGISTERED_USERS(id, user_name, password, post_code, city, address, loyalty_card_number, enabled) VALUES
         (1001, 'john_doe', '$2a$10$NTaRfLPAggoqgoDalCcC/O9jN4pTzYS30gDVQgcMLPCvaFT0AdTJy', '1100', 'Budapest', 'Kossuth Lajos utca 1.', 1001, true),
         (1002, 'jane_doe', '$2a$10$kCSV2ySvzlLab5BSvb26P.S87NwD7ettC25.EtsYm..sTtc6KGvx2', '1100', 'Budapest', 'Kossuth Lajos utca 2.', 1002, true),
         (1003, 'admin', '$2a$10$lcajsVzd3UqKdn.8A/RPjOBb99LCR2S1q7S5QZN6oN0hwCi.OiHvS', '1100', 'Budapest', 'Kossuth Lajos utca 3.', NULL, true);

INSERT INTO RESERVATIONS(id, app_user_id, date, start_time, end_time, number_of_guests, time_of_booking, version, created, updated) VALUES
        (1001, 1004, '2024-05-01', '18:00', '20:00', 3, NULL, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1002, 1003, '2024-05-01', '19:00', '21:00', 4, NULL, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1003, 1002, '2024-05-01', '19:30', '21:00', 2, NULL, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1004, 1001, '2024-05-01', '19:00', '22:00', 5, NULL, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO RESTAURANT_TABLES(id, table_number, max_number_of_seats, version, created, updated) VALUES
        (1001, 1, 2, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1002, 2, 4, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1003, 3, 4, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1004, 4, 6, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1005, 5, 2, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1006, 6, 4, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1007, 7, 4, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
        (1008, 8, 8, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO USER_ROLES(user_id, role_id) VALUES
        (1001, 1),
        (1002, 1),
        (1003, 2);