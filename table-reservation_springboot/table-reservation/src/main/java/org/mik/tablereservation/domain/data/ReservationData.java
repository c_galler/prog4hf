package org.mik.tablereservation.domain.data;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.mik.tablereservation.domain.AppUser;
import org.mik.tablereservation.domain.Reservation;

@Data
@NoArgsConstructor
public class ReservationData extends AbstractData<Long> {
    @NotNull
    private Reservation reservation;
    @NotNull
    private AppUser newUser;
}
