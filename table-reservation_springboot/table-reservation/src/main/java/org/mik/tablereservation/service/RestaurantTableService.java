package org.mik.tablereservation.service;

import org.mik.tablereservation.domain.RestaurantTable;
import org.mik.tablereservation.repository.RestaurantTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantTableService {
    private RestaurantTableRepository restaurantTableRepository;

    @Autowired
    public void setRestaurantTableRepository(RestaurantTableRepository restaurantTableRepository) {
        this.restaurantTableRepository = restaurantTableRepository;
    }
    public List<RestaurantTable> getAllTables() {
        return restaurantTableRepository.findAll();
    }

    public RestaurantTable createTable(RestaurantTable table) {
        return restaurantTableRepository.save(table);
    }

    public RestaurantTable updateTable(Long id, RestaurantTable table) {
        RestaurantTable existingTable = restaurantTableRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Table with id " + id + " not found"));

        existingTable.setTableNumber(table.getTableNumber());
        existingTable.setMaxNumberOfSeats(table.getMaxNumberOfSeats());
        return restaurantTableRepository.save(existingTable);
    }

    public void deleteTable(Long id) {
        restaurantTableRepository.deleteById(id);
    }
}
