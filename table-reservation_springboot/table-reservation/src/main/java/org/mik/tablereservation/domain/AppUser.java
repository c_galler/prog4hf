package org.mik.tablereservation.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity
@Table(name = "app_users")
public class AppUser extends AbstractEntity<Long> {
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;

    //@JsonBackReference
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "appUser")
    private List<Reservation> reservations;

    public void addReservation(Reservation reservation) {
        this.reservations.add(reservation);
    }
}
