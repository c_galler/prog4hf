package org.mik.tablereservation.repository;

import org.mik.tablereservation.domain.RestaurantTable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RestaurantTableRepository extends CrudRepository<RestaurantTable, Long> {
    List<RestaurantTable> findAll();
    Optional<RestaurantTable> findByTableNumber(int tableNumber);
    Optional<RestaurantTable> findById(Long id);

    RestaurantTable findByMaxNumberOfSeatsGreaterThanEqual(Long requiredNumberOfSeats);
}
