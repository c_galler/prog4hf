package org.mik.tablereservation.controller.rest;

public class ResourceAlreadyExistsException extends RuntimeException {

    public ResourceAlreadyExistsException(String s) {
        super(s);
    }
}
