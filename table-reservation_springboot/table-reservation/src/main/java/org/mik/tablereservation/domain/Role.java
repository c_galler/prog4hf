package org.mik.tablereservation.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.GrantedAuthority;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "roles")
@Data
@Table
@SuperBuilder
@NoArgsConstructor
public class Role extends AbstractEntity<Long> implements GrantedAuthority {

    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Override
    public String getAuthority() {
        return name;
    }

}