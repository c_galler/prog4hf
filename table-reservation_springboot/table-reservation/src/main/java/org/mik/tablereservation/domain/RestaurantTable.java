package org.mik.tablereservation.domain;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@Entity(name = "restaurant_tables")
public class RestaurantTable extends AbstractEntity<Long> {
    private int tableNumber;
    private int maxNumberOfSeats;

    @ManyToMany(mappedBy = "restaurantTables")
    private List<Reservation> reservations;
}
