package org.mik.tablereservation.repository;

import org.mik.tablereservation.domain.RegisteredAppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RegisteredUserRepository extends JpaRepository<RegisteredAppUser, Long> {
    List<RegisteredAppUser> findAll();
    RegisteredAppUser findByUserNameAndEnabled(String userName, boolean enabled);

    Optional<RegisteredAppUser> findByUserName(String username);
}
