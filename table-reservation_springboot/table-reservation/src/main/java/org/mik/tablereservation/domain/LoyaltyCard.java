package org.mik.tablereservation.domain;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@AttributeOverride(name="id", column=@Column(name="card_number"))
public class LoyaltyCard extends AbstractEntity<Long> {
    private int points;
    private Date expirationDate;

    @OneToOne(mappedBy = "loyaltyCard")
    private RegisteredAppUser owner;

    public LoyaltyCard(int points, Date expirationDate) {
        this.points = points;
        this.expirationDate = expirationDate;
    }
}
