package org.mik.tablereservation.controller.rest;

import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@RestControllerAdvice
@Log4j2
public class RestExceptionHandler {

    @ExceptionHandler(ResourceNotfoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage resourceNotFoundException(ResourceNotfoundException ex, WebRequest request) {
        log.warn(request.getDescription(true), ex);
        return new ErrorMessage(
                HttpStatus.NOT_FOUND.value(),
                LocalDateTime.now(),
                ex.getMessage(),
                request.getDescription(false));
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage resourceAlreadyExistsException(ResourceAlreadyExistsException ex, WebRequest request) {
        log.warn(request.getDescription(true), ex);
        return new ErrorMessage(
                HttpStatus.BAD_REQUEST.value(),
                LocalDateTime.now(),
                ex.getMessage(),
                request.getDescription(false));
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT)
    public ErrorMessage handleDataIntegrityViolation (DataIntegrityViolationException ex, WebRequest req) {
        log.error(req.getDescription(true), ex);
        return new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                LocalDateTime.now(),
                ex.getMessage(),
                req.getDescription(false));
    }

    @ExceptionHandler({AccessDeniedException.class})
    public @ResponseBody String accessDenied(AccessDeniedException e, WebRequest request, Model model) {
        log.warn("Access denied");
        model.addAttribute("error", "Access denied");
        return "redirect:/";
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage serverError(Exception e, WebRequest req) {
        log.error(req.getDescription(true), e);
        return new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                LocalDateTime.now(),
                "Server error, see log",
                req.getDescription(false));
    }


}

