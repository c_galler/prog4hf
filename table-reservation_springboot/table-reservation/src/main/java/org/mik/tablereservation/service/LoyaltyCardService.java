package org.mik.tablereservation.service;

import org.mik.tablereservation.domain.LoyaltyCard;
import org.mik.tablereservation.repository.LoyaltyCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LoyaltyCardService {
    private LoyaltyCardRepository loyaltyCardRepository;

    @Autowired
    public void setLoyaltyCardRepository(LoyaltyCardRepository loyaltyCardRepository) {
        this.loyaltyCardRepository = loyaltyCardRepository;
    }

    public Optional<LoyaltyCard> findByCardNumber(Long cardNumber) {
        return loyaltyCardRepository.findById(cardNumber);
    }

    public void addPoints(Long cardNumber, int points) {
        Optional<LoyaltyCard> optionalLoyaltyCard = findByCardNumber(cardNumber);
        if(optionalLoyaltyCard.isEmpty()) {
            return;
        }
        LoyaltyCard loyaltyCard = optionalLoyaltyCard.get();
        loyaltyCard.setPoints(loyaltyCard.getPoints() + points);
        loyaltyCardRepository.save(loyaltyCard);
    }

    public LoyaltyCard getLoyaltyCardByOwner(Long ownerId) {
        return loyaltyCardRepository.findByOwnerId(ownerId);
    }

    public LoyaltyCard save(LoyaltyCard loyaltyCard) {
        return loyaltyCardRepository.save(loyaltyCard);
    }

    public List<LoyaltyCard> getAllLoyaltyCards() {
        return loyaltyCardRepository.findAll();
    }

    public LoyaltyCard createLoyaltyCard(LoyaltyCard loyaltyCard) {
        return loyaltyCardRepository.save(loyaltyCard);
    }

    public LoyaltyCard updateLoyaltyCard(Long id, LoyaltyCard loyaltyCard) {
        LoyaltyCard existingLoyaltyCard = loyaltyCardRepository.findById(id)
                .orElseThrow( () -> new IllegalArgumentException("Loyalty card with id " + id + " not found"));

        existingLoyaltyCard.setOwner(loyaltyCard.getOwner());
        existingLoyaltyCard.setPoints(loyaltyCard.getPoints());

        return loyaltyCardRepository.save(existingLoyaltyCard);
    }

    public void deleteLoyaltyCard(Long id) {
        loyaltyCardRepository.deleteById(id);
    }
}