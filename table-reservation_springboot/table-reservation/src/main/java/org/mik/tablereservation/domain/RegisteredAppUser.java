package org.mik.tablereservation.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity(name = RegisteredAppUser.TABLE_NAME)
@SuperBuilder
public class RegisteredAppUser extends AppUser {
    public static final String TABLE_NAME = "registered_users";

    @Column(nullable = false)
    @NotNull(message ="Username cannot be null")
    @Size(min = 1, max = 50, message = "Length must be between 1 and 50")
    private  String userName;

    @Column(nullable = false)
    @NotNull(message ="Password cannot be null")
    @Size(min = 1, max = 255, message = "Length must be between 1 and 255")
    private String password;

    private String address;
    private String city;
    private String postCode;
    private LocalDate dateOfBirth;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "loyalty_card_number")
    private LoyaltyCard loyaltyCard;

    @Column(nullable = false)
    private boolean enabled;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_roles",
            joinColumns = {@JoinColumn(name="user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")}
    )
    private List<Role> roles;

    public RegisteredAppUser(String userName, String password, boolean enabled, List<Role> roles) {
        this.userName = userName;
        this.password = password;
        this.enabled = enabled;
        this.roles = roles;
    }
}
