package org.mik.tablereservation.service;

import org.mik.tablereservation.domain.RegisteredAppUser;
import org.mik.tablereservation.repository.RegisteredUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.security.core.userdetails.User;

import java.util.List;

@Service
public class RegisteredUserService implements UserDetailsService {
    private RegisteredUserRepository registeredUserRepository;
    private PasswordEncoder passwordEncoder;
    @Autowired
    public void setRegisteredUserRepository(RegisteredUserRepository registeredUserRepository) {
        this.registeredUserRepository = registeredUserRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public List<RegisteredAppUser> getAllRegisteredUsers() {
        return registeredUserRepository.findAll();
    }

    public RegisteredAppUser getUser(Long id) {
        return registeredUserRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("User with id " + id + " not found"));
    }
    public RegisteredAppUser createUser(RegisteredAppUser registeredAppUser) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(registeredAppUser.getPassword());
        registeredAppUser.setPassword(encodedPassword);
        return registeredUserRepository.save(registeredAppUser);
    }

    public String registerUser(RegisteredAppUser sentUser) {
        String encodedPassword = passwordEncoder.encode(sentUser.getPassword());
        var newUser = RegisteredAppUser.builder()
                .firstName(sentUser.getFirstName())
                .lastName(sentUser.getLastName())
                .email(sentUser.getEmail())
                .phoneNumber(sentUser.getPhoneNumber())
                .userName(sentUser.getUserName())
                .password(encodedPassword)
                .enabled(true)
                .roles(sentUser.getRoles())
                .build();
        var savedUser = registeredUserRepository.save(newUser);
        return "User's been successfully registered";
    }

    public RegisteredAppUser updateUser(Long id, RegisteredAppUser sentUser) {
        RegisteredAppUser existingUser = registeredUserRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("User with id " + id + " not found"));

        String encodedPassword = passwordEncoder.encode(sentUser.getPassword());
        var updatedUser = RegisteredAppUser.builder()
                .firstName(sentUser.getFirstName())
                .lastName(sentUser.getLastName())
                .email(sentUser.getEmail())
                .phoneNumber(sentUser.getPhoneNumber())
                .userName(sentUser.getUserName())
                .password(encodedPassword)
                .enabled(sentUser.isEnabled())
                .roles(sentUser.getRoles())
                .build();

        updatedUser.setId(existingUser.getId()); // Preserve the id of the existing user

        return registeredUserRepository.save(updatedUser);
    }

    public void deleteUser(Long id) {
        registeredUserRepository.deleteById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (!StringUtils.hasLength(username) || username.isBlank())
            throw new UsernameNotFoundException("Empty username");

        RegisteredAppUser registeredUser = this.registeredUserRepository.findByUserName(username)
                .orElseThrow(()->new UsernameNotFoundException("User not found"));

        return new User(registeredUser.getUserName(), registeredUser.getPassword(), registeredUser.getRoles());
    }

}
