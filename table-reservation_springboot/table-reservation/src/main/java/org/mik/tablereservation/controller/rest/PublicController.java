package org.mik.tablereservation.controller.rest;

import jakarta.validation.constraints.NotNull;
import org.mik.tablereservation.domain.RegisteredAppUser;
import org.mik.tablereservation.domain.Reservation;
import org.mik.tablereservation.domain.data.ReservationData;
import org.mik.tablereservation.service.RegisteredUserService;
import org.mik.tablereservation.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins="*", maxAge=3600)
@RequestMapping("/api")
public class PublicController {
    private ReservationService reservationService;
    private RegisteredUserService registeredUserService;

    @Autowired
    public void setReservationService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @Autowired
    public void setRegisteredUserService(RegisteredUserService registeredUserService) {
        this.registeredUserService = registeredUserService;
    }

    // Home page
    @GetMapping({"/", "/home"})
    public ResponseEntity<Map<String, String>> getHomePage() {
        Map<String, String> response = new HashMap<>();
        response.put("message", "Welcome to our restaurant's table reservation system!");
        return ResponseEntity.ok(response);
    }

    @PostMapping("/reservations")
    public ResponseEntity<Reservation> createReservation(@NotNull @RequestBody ReservationData reservationData) {
        return ResponseEntity.ok(reservationService.saveReservationForUnregisteredUser(reservationData.getReservation(), reservationData.getNewUser()));
    }

    // User registration
    @PostMapping("/register")
    public ResponseEntity<RegisteredAppUser> registerUser(@NotNull @RequestBody RegisteredAppUser newUser) {
        if (newUser.getId() != null) {
            throw new ResourceAlreadyExistsException("User already exists with id: " + newUser.getId());
        }
        return ResponseEntity.ok(registeredUserService.createUser(newUser));
    }




}
