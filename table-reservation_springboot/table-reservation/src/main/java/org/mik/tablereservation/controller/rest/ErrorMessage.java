package org.mik.tablereservation.controller.rest;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ErrorMessage {

    private int statusCode;
    private LocalDateTime tstamp;
    private String message;
    private String description;
}
