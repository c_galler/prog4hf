package org.mik.tablereservation.controller.rest;

public class ResourceNotfoundException extends RuntimeException {

    public ResourceNotfoundException(String message) {
        super(message);
    }

}
