package org.mik.tablereservation.repository;

import org.mik.tablereservation.domain.Reservation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {
    List<Reservation> findAll();
    Optional<Reservation> findById(Long id);
    List<Reservation> findByAppUserId(Long userId);
}