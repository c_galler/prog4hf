package org.mik.tablereservation.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Data
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity(name = "reservations")
public class Reservation extends AbstractEntity<Long> {
    @Temporal(TemporalType.DATE)
    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;
    private LocalDateTime timeOfBooking;
    private int numberOfGuests;

    //@JsonManagedReference
    @ManyToOne
    private AppUser appUser;

    @ManyToMany
    @JoinTable(
            joinColumns = @JoinColumn(name = "reservation_id"),
            inverseJoinColumns = @JoinColumn(name = "restaurant_table_id"))
    private List<RestaurantTable> restaurantTables;
    private String comment;
}
