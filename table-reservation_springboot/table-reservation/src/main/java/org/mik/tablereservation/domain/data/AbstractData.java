package org.mik.tablereservation.domain.data;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@SuperBuilder
public class AbstractData<ID> {
    private ID id;
    private int version;
    private LocalDateTime updated;
    private LocalDateTime created;
}