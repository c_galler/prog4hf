package org.mik.tablereservation.repository;

import org.mik.tablereservation.domain.LoyaltyCard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LoyaltyCardRepository extends CrudRepository<LoyaltyCard, Long> {
    Optional<LoyaltyCard> findById(Long id);

    LoyaltyCard findByOwnerId(Long ownerId);

    List<LoyaltyCard> findAll();
}
