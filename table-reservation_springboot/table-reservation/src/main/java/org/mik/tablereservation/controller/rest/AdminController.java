package org.mik.tablereservation.controller.rest;

import org.mik.tablereservation.domain.LoyaltyCard;
import org.mik.tablereservation.domain.RegisteredAppUser;
import org.mik.tablereservation.domain.Reservation;
import org.mik.tablereservation.domain.RestaurantTable;
import org.mik.tablereservation.service.LoyaltyCardService;
import org.mik.tablereservation.service.RegisteredUserService;
import org.mik.tablereservation.service.ReservationService;
import org.mik.tablereservation.service.RestaurantTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin(origins="*", maxAge=3600)
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/admin")
public class AdminController {
    private RegisteredUserService registeredUserService;
    private RestaurantTableService restaurantTableService;
    private ReservationService reservationService;
    private LoyaltyCardService loyaltyCardService;

    @Autowired
    public void setRegisteredUserService(RegisteredUserService registeredUserService) {
        this.registeredUserService = registeredUserService;
    }

    @Autowired
    public void setRestaurantTableService(RestaurantTableService restaurantTableService) {
        this.restaurantTableService = restaurantTableService;
    }

    @Autowired
    public void setReservationService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

     @Autowired
    public void setLoyaltyCardService(LoyaltyCardService loyaltyCardService) {
        this.loyaltyCardService = loyaltyCardService;
    }

    // Users
    @GetMapping("/userManagement/users")
    public List<RegisteredAppUser> getAllUsers() {
        return registeredUserService.getAllRegisteredUsers();
    }

    @PostMapping("/userManagement/users")
    public RegisteredAppUser createUser(@RequestBody RegisteredAppUser registeredAppUser) {
        return registeredUserService.createUser(registeredAppUser);
    }

    @GetMapping("/userManagement/users/{id}")
    public RegisteredAppUser getUser(@PathVariable Long id) {
        return registeredUserService.getUser(id);
    }

    @PutMapping("/userManagement/users/{id}")
    public RegisteredAppUser updateUser(@PathVariable Long id, @RequestBody RegisteredAppUser registeredAppUser) {
        return registeredUserService.updateUser(id, registeredAppUser);
    }

    @DeleteMapping("/userManagement/users/{id}")
    public void deleteUser(@PathVariable Long id) {
        registeredUserService.deleteUser(id);
    }

    // Tables
    @GetMapping("/tableManagement/tables")
    public List<RestaurantTable> getAllTables() {
        return restaurantTableService.getAllTables();
    }

    @PostMapping("/tableManagement/tables")
    public RestaurantTable createTable(@RequestBody RestaurantTable table) {
        return restaurantTableService.createTable(table);
    }

    @PutMapping("/tableManagement/tables/{id}")
    public RestaurantTable updateTable(@PathVariable Long id, @RequestBody RestaurantTable table) {
        return restaurantTableService.updateTable(id, table);
    }

    @DeleteMapping("/tableManagement/tables/{id}")
    public void deleteTable(@PathVariable Long id) {
        restaurantTableService.deleteTable(id);
    }

    // Reservations
    @GetMapping("/reservationManagement/reservations")
    public List<Reservation> getAllReservations() {
        return reservationService.getReservations();
    }

    @PostMapping("/reservationManagement/reservations")
    public void createReservation(@RequestBody Reservation reservation) {
        reservationService.saveReservationForAdmin(reservation);
    }

    @GetMapping("/reservationManagement/reservations/{id}")
    public Reservation getReservation(@PathVariable Long id) {
        return reservationService.getReservation(id);
    }

    @PutMapping("/reservationManagement/reservations/{id}")
    public Reservation updateReservation(@PathVariable Long id, @RequestBody Reservation reservation) {
        return reservationService.updateReservation(id, reservation);
    }

    @DeleteMapping("/reservationManagement/reservations/{id}")
    public void deleteReservation(@PathVariable Long id) {
        reservationService.deleteReservation(id);
    }

    // Loyalty cards
    @GetMapping("/loyaltyCardManagement/loyaltyCards")
    public List<LoyaltyCard> getAllLoyaltyCards() {
        return loyaltyCardService.getAllLoyaltyCards();
    }

    @PostMapping("/loyaltyCardManagement/loyaltyCards")
    public LoyaltyCard createLoyaltyCard(@RequestBody LoyaltyCard loyaltyCard) {
        return loyaltyCardService.createLoyaltyCard(loyaltyCard);
    }

    @PutMapping("/loyaltyCardManagement/loyaltyCards/{id}")
    public LoyaltyCard updateLoyaltyCard(@PathVariable Long id, @RequestBody LoyaltyCard loyaltyCard) {
        return loyaltyCardService.updateLoyaltyCard(id, loyaltyCard);
    }

    @DeleteMapping("/loyaltyCardManagement/loyaltyCards/{id}")
    public void deleteLoyaltyCard(@PathVariable Long id) {
        loyaltyCardService.deleteLoyaltyCard(id);
    }
}
