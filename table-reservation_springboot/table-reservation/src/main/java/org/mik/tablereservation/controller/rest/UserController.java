package org.mik.tablereservation.controller.rest;

import org.mik.tablereservation.domain.LoyaltyCard;
import org.mik.tablereservation.domain.RegisteredAppUser;
import org.mik.tablereservation.domain.Reservation;
import org.mik.tablereservation.service.LoyaltyCardService;
import org.mik.tablereservation.service.RegisteredUserService;
import org.mik.tablereservation.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin(origins="*", maxAge=3600)
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/user")
public class UserController {

    private RegisteredUserService registeredUserService;
    private ReservationService reservationService;
    private LoyaltyCardService loyaltyCardService;

    @Autowired
    public void setRegisteredUserService(RegisteredUserService registeredUserService) {
        this.registeredUserService = registeredUserService;
    }
    @Autowired
    public void setReservationRepository(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @Autowired
    public void setLoyaltyCardService(LoyaltyCardService loyaltyCardService) {
        this.loyaltyCardService = loyaltyCardService;
    }

    @GetMapping("/{id}")
    public RegisteredAppUser getUser(@PathVariable Long id) {
        return registeredUserService.getUser(id);
    }

    @PutMapping("/{id}")
    public RegisteredAppUser updateUser(@PathVariable Long id, @RequestBody RegisteredAppUser registeredAppUser) {
        return registeredUserService.updateUser(id, registeredAppUser);
    }

    @GetMapping("/{id}/reservations")
    public List<Reservation> getUserReservations(@PathVariable Long id) {
        return reservationService.getReservationsByUserId(id);
    }

    @PostMapping("/{id}/reservations")
    public Reservation createReservation(@PathVariable Long id, @RequestBody Reservation reservation) {
        return reservationService.saveReservationForRegisteredUser(reservation, id);
    }

    @PutMapping("/{id}/reservations/{reservationId}")
    public Reservation updateReservation(@PathVariable Long id, @PathVariable Long reservationId, @RequestBody Reservation reservation) {
        return reservationService.updateReservation(reservationId, reservation);
    }

    @DeleteMapping("/{id}/reservations/{reservationId}")
    public void deleteReservation(@PathVariable Long id, @PathVariable Long reservationId) {
        reservationService.deleteReservation(reservationId);
    }

    @GetMapping("/{id}/loyaltycard")
    public LoyaltyCard getUserLoyaltyCard(@PathVariable Long id) {
        return loyaltyCardService.getLoyaltyCardByOwner(id);
    }
}

