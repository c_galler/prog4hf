package org.mik.tablereservation.service;

import org.mik.tablereservation.domain.AppUser;
import org.mik.tablereservation.domain.RegisteredAppUser;
import org.mik.tablereservation.domain.Reservation;
import org.mik.tablereservation.repository.AppUserRepository;
import org.mik.tablereservation.repository.RegisteredUserRepository;
import org.mik.tablereservation.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReservationService {
    private ReservationRepository reservationRepository;
    private AppUserRepository appUserRepository;
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    public void setReservationRepository(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Autowired
    public void setAppUserRepository(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Autowired
    public  void  setRegisteredUserRepository(RegisteredUserRepository registeredUserRepository) {
        this.registeredUserRepository = registeredUserRepository;
    }

    public List<Reservation> getReservations() {
        return reservationRepository.findAll();
    }

    public List<Reservation> getReservationsByAppUser(Long userId) {
        return reservationRepository.findByAppUserId(userId);
    }

    public Reservation saveReservationForAdmin(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    public Reservation saveReservationForRegisteredUser(Reservation reservation, Long registeredUserId) {
        // Fetch the RegisteredUser from the database
        RegisteredAppUser registeredUser = registeredUserRepository.findById(registeredUserId)
                .orElseThrow(() -> new IllegalArgumentException("RegisteredUser with id " + registeredUserId + " not found"));

        reservation.setAppUser(registeredUser);
        return reservationRepository.save(reservation);
    }

    public Reservation saveReservationForUnregisteredUser(Reservation reservation, AppUser newAppUser) {
        // Create a new AppUser

        appUserRepository.save(newAppUser);
        reservation.setAppUser(newAppUser);
        return reservationRepository.save(reservation);
    }

    public Reservation updateReservation(Long id, Reservation reservation) {
        Reservation existingReservation = reservationRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Reservation with id " + id + " not found"));

        AppUser existingAppUser = existingReservation.getAppUser();

        // Only update the AppUser if it's not a RegisteredAppUser
        if (!(existingAppUser instanceof RegisteredAppUser)) {
            existingAppUser.setFirstName(reservation.getAppUser().getFirstName());
            existingAppUser.setLastName(reservation.getAppUser().getLastName());
            existingAppUser.setEmail(reservation.getAppUser().getEmail());
            existingAppUser.setPhoneNumber(reservation.getAppUser().getPhoneNumber());

            // Save the updated AppUser back to the database
            appUserRepository.save(existingAppUser);
        }

        // Update the Reservation fields
        existingReservation.setDate(reservation.getDate());
        existingReservation.setStartTime(reservation.getStartTime());
        existingReservation.setNumberOfGuests(reservation.getNumberOfGuests());
        existingReservation.setComment(reservation.getComment());
        existingReservation.setRestaurantTables(reservation.getRestaurantTables());

        // Save the updated Reservation back to the database
        return reservationRepository.save(existingReservation);

    }

    private ArrayList<String> validateReservation(Reservation reservation) {
        ArrayList<String> errors = new ArrayList<>();
        if (reservation == null) {
            errors.add("Reservation data is required");
        } else {
            // Date and time validations
            LocalDate currentDate = LocalDate.now();
            if (reservation.getDate() == null || reservation.getDate().isBefore(currentDate)) {
                errors.add("Reservation date cannot be in the past");
            }
            // Guest count validation
            int minGuests = 1;
            int maxGuests = 12;
            if (reservation.getNumberOfGuests() < minGuests || reservation.getNumberOfGuests() > maxGuests) {
                errors.add("Number of guests must be between " + minGuests + " and " + maxGuests);
            }

            // User validation
        }

        return errors;
    }

    public void deleteReservation(Long id) {
        reservationRepository.deleteById(id);
    }

    public Reservation getReservation(Long id) {
        return reservationRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Reservation with id " + id + " not found"));
    }

    public List<Reservation> getReservationsByUserId(Long id) {
        return reservationRepository.findByAppUserId(id);
    }
}
